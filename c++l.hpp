#include <type_traits>
#include <iostream>
#include <string>

namespace c_plus_plus_l {

/*
Formulas
*/

//Variables
template<char Letter> 
class Variable {
public:
    static const char Label = Letter;
    static const bool is_variable=true;
    static const bool is_formula=true;
    static const bool is_provable=true;
    operator std::string () const {return std::string()+Letter;};
};

Variable<'A'> A; 
Variable<'B'> B;
Variable<'C'> C;
Variable<'D'> D;


//Implication
template<typename A, typename B> 
        requires A::is_formula && B::is_formula
class Implication {
    A a_;
    B b_;
public:
    typedef A left;
    typedef B right;
    static const bool is_implication=true;
    static const bool is_formula=true;
    static const bool is_provable=true;
    Implication(A a, B b): a_(a), b_(b) {};
    operator std::string () const {return std::string("(")+std::string(a_)+"->"+std::string(b_)+")";};
    auto get_left() const {return a_;};
    auto get_right() const {return b_;};
};

template<typename A, typename B> 
        requires A::is_formula && B::is_formula
auto operator >> (A a, B b) {return Implication(a,b);}


//Negation
template<typename A> 
        requires A::is_formula
class Negation {
    A a_;
public:
    typedef A formula;
    static const bool is_negation=true;
    static const bool is_formula=true;
    static const bool is_provable=true;
    Negation(A a): a_(a) {};
    operator std::string () const {return std::string("!")+std::string(a_);};
    auto get() const {return a_;};
};

template<typename A> 
        requires A::is_formula
auto operator !(A a) {return Negation(a);}


//Comma
template<typename A, typename B> 
        requires (A::is_formula || A::is_comma) && B::is_formula
class Comma {
    A a_;
    B b_;
public:
    typedef A left;
    typedef B right;
    static const bool is_comma=true;
    Comma(A a, B b): a_(a), b_(b) {};
    operator std::string () const {return std::string("(")+std::string(a_)+"->"+std::string(b_)+")";};
    auto get_left() const {return a_;};
    auto get_right() const {return b_;};
};

template<typename A, typename B> 
        requires A::is_formula && (B::is_formula || !B::is_comma)
auto operator , (A a, B b) {return Comma(a,b);}


//Deduction
template<typename A, typename B> 
        requires (A::is_formula || A::is_comma) && B::is_formula
class Deduction {
    A a_;
    B b_;
public:
    typedef A left;
    typedef B right;
    static const bool is_provable=true;
    static const bool is_formula=false;
    Deduction(A a, B b): a_(a), b_(b) {};
    operator std::string () const {return std::string("(")+std::string(a_)+"->"+std::string(b_)+")";};
    auto get_left() const {return a_;};
    auto get_right() const {return b_;};
};

template<typename A, typename B> 
        requires (A::is_formula || A::is_comma)  && B::is_formula
auto operator |= (A a, B b) {return Deduction(a,b);} 


//Deduction utilities get_formula

template<typename A, typename B>
struct get_formula_helper : std::false_type {};

template<typename A>
        requires A::is_formula
struct get_formula_helper<A, A> {
    typedef A formula;    
    static formula to_formula(A a) {return a;}
};

template<typename A>
        requires A::is_provable && !A::is_formula
struct get_formula_helper<A, A> {
    typedef typename A::right formula;    
    static formula to_formula(A a) {return a.get_right();}
};

template<typename A>
        requires A::is_provable
using get_formula = get_formula_helper<A, A>;


/*
Proven stuff
*/

int deduction_counter=0;
template<typename B, typename R>
        requires R::is_reason && B::is_provable
class IsProven; //I have issues with template operators, so I introduce this auxiliary


template <typename A>
        requires A::is_provable
class Proven {
    A f_;
    int ide_;    
    Proven(A f, int ide): f_(f), ide_(ide) {};
public:
    static const bool is_proven=true;   
    typedef A formula;    

    template<typename B, typename R>
        requires R::is_reason && B::is_provable
    friend class IsProven;

    Proven(const Proven& p): f_(p.f_), ide_(p.ide_) {};
    Proven & operator=(const Proven& p) {f_=p.f_; return *this;};    

    int get_id() const {return ide_;}
    auto get() const {return f_;};
};

template<typename B, typename R>
        requires R::is_reason && B::is_provable
class IsProven {
public:
    static Proven<B> to_proven(B f, int ide) {
        return Proven<B>(f, ide);
    }
};

template<typename B, typename R>
        requires R::is_reason && B::is_provable
Proven<B> operator<< (B a, R r) {
    ++deduction_counter;
    std::cout<<std::to_string(deduction_counter)<<". "<< std::string(a) << "\t" << std::string(r) <<"\n";
    return IsProven<B,R>::to_proven(r.evaluate(a), deduction_counter);
}





/*
Reasons
*/

//Modus ponens
template<typename A, typename B> 
        requires A::is_proven && B::is_proven 
              && get_formula<typename B::formula>::formula::is_implication 
              && std::is_same<typename get_formula<typename A::formula>::formula, 
                              typename get_formula<typename B::formula>::formula::left
                             >::value
class ModusPonens {
    A a_;
    B b_;

public:
    static const bool is_reason=true;   

    ModusPonens(A a, B b): a_(a), b_(b) {};

    template<typename P> requires P::is_provable
    auto evaluate(P p) const {
        auto conclusion=get_formula<typename B::formula>::to_formula(b_.get()).get_right();
        if constexpr (P::is_formula) {(void)p; return conclusion;} 
        else return Deduction(p.get_left(),conclusion);
    }

    operator std::string () const {return std::string("Modus ponens: (")+std::to_string(a_.get_id())
                                                              +") and ("+std::to_string(b_.get_id())+")";}
};

template<typename A, typename B> 
        requires A::is_formula && B::is_formula
class Axiom1 {
    A a_;
    B b_;
public:
    static const bool is_reason=true;   

    Axiom1(A a, B b): a_(a), b_(b) {};

    template<typename P> requires P::is_provable
    auto evaluate(P p) const {
        auto conclusion=a_>>(b_>>a_);
        if constexpr (P::is_formula) {(void)p; return conclusion;} 
        else return Deduction(p.get_left(),conclusion);
    }

    operator std::string () const {return std::string("Axiom 1: A=")+std::string(a_)+" B="+std::string(b_);};
};

template<typename A, typename B, typename C> 
        requires A::is_formula && B::is_formula && C::is_formula
class Axiom2 {
    A a_;
    B b_;
    C c_;
public:
    static const bool is_reason=true;   

    Axiom2(A a, B b, C c): a_(a), b_(b), c_(c) {}

    template<typename P> requires P::is_provable
    auto evaluate(P p) const {
        auto conclusion=(a_>>(b_>>c_))>>((a_>>b_)>>(a_>>c_));
        if constexpr (P::is_formula) {(void)p; return conclusion;} 
        else return Deduction(p.get_left(),conclusion);
    }

    operator std::string () const {return std::string("Axiom 2: A=")+std::string(a_)+" B="+std::string(b_)+" C="+std::string(c_);}
};


template<typename A, typename B> 
        requires A::is_formula && B::is_formula
class Axiom3 {
    A a_;
    B b_;
public:
    static const bool is_reason=true;   

    Axiom3(A a, B b): a_(a), b_(b) {};
    
    template<typename P> requires P::is_provable
    auto evaluate(P p) const {
        auto conclusion=(!a_>>!b_)>>(b_>>a_);
        if constexpr (P::is_formula) {(void)p; return conclusion; }
        else return Deduction(p.get_left(),conclusion);
    }

    operator std::string () const {return std::string("Axiom 3: A=")+std::string(a_)+" B="+std::string(b_);};
};







} //namespace c_plus_plus_l end

