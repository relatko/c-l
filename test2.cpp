#include "c++l.hpp"

using namespace c_plus_plus_l;


int main() {

auto s1 =(    (A, B) |= (!A>>!B)>>(B>>A)             )<< Axiom3(A,B);

typedef Implication<Variable<'A'>,Variable<'B'>> impl;
static_assert(std::is_same<impl,get_formula<impl>::formula>::value);
static_assert(std::is_same<impl,get_formula<Deduction<impl, impl>>::formula>::value);

/*
auto s1 =(         (A, B) |= A             )<< FromDeductionSet<0>();
auto s2 =(             A  |= B>>A          )<< DeductionTheorem<1>(s1);
auto s3 =(                   A>>(B>>A)	   )<< DeductionTheorem<0>(s2);
*/

}
