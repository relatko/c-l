DBGFLAGS = -g -O0 -fconcepts -Wall -Wextra -pedantic
CFLAGS = -std=c++17 $(DBGFLAGS)

test1: 
	$(CXX) $(CFLAGS) test1.cpp -o a.out
	./a.out

test2: 
	$(CXX) $(CFLAGS) test2.cpp -o a.out
	./a.out


.PHONY: test1 test2
